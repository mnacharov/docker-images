ARG ALPINE_VERSION=3.18.4
FROM alpine:$ALPINE_VERSION as build
LABEL org.opencontainers.image.authors="mike.nacharov@gmail.com"

WORKDIR /build
ARG VERSION=0.4.8.9

# python3 installed to run tor unittests before build
RUN apk update -q \
 && apk upgrade -q \
 && apk add -q --no-cache \
      g++ \
      make \
      python3 \
      zstd-dev \
      openssl-dev \
      libevent-dev \
      zlib-dev

###~ Tor ~###
RUN wget -q https://dist.torproject.org/tor-$VERSION.tar.gz \
            https://dist.torproject.org/tor-$VERSION.tar.gz.sha256sum \
 && sha256sum -c tor-$VERSION.tar.gz.sha256sum \
 && tar -xf tor-$VERSION.tar.gz

RUN cd tor-$VERSION \
 && ./configure \
      --disable-asciidoc \
      --disable-html-manual \
      --disable-lzma \
      --disable-manpage \
      --disable-seccomp \
      --disable-system-torrc \
      --disable-systemd \
      --prefix /build/tor \
 && make \
 && make install

FROM alpine:$ALPINE_VERSION as run

COPY --from=build /build/tor /usr/local

RUN apk update -q \
 && apk upgrade -q \
 && apk add -q --no-cache \
      openssl \
      libevent \
      zstd-libs \
 && mkdir -m 700 /data \
 && chown -R 65534:65534 /data

USER 65534:65534
VOLUME ["/data"]
EXPOSE 9050 9051

ENTRYPOINT ["/usr/local/bin/tor", "DataDirectory", "/data", "SocksPort", "0.0.0.0:9050", "ControlPort", "0.0.0.0:9051"]
