ARG DEBIAN_VERSION=12.2
FROM debian:$DEBIAN_VERSION as build
LABEL org.opencontainers.image.authors="mike.nacharov@gmail.com"

WORKDIR /build
ARG VERSION=0.4.8.9

# python3 installed to run tor unittests before build
RUN apt-get update -qq \
 && apt-get upgrade -yqq \
 && apt-get install -yqq \
      build-essential \
      libzstd-dev \
      libssl-dev \
      libevent-dev \
      zlib1g-dev \
      python3 \
      wget

###~ Tor ~###
RUN wget -q https://dist.torproject.org/tor-$VERSION.tar.gz \
            https://dist.torproject.org/tor-$VERSION.tar.gz.sha256sum \
 && sha256sum -c tor-$VERSION.tar.gz.sha256sum \
 && tar -xf tor-$VERSION.tar.gz

RUN cd tor-$VERSION \
 && ./configure \
      --disable-asciidoc \
      --disable-html-manual \
      --disable-lzma \
      --disable-manpage \
      --disable-seccomp \
      --disable-system-torrc \
      --disable-systemd \
      --enable-fatal-warnings \
      --prefix /build/tor \
 && make \
 && make install

FROM debian:$DEBIAN_VERSION-slim as run

COPY --from=build /build/tor /usr/local

RUN apt-get update -qq \
 && apt-get upgrade -yqq \
 && apt-get install -yqq \
      libssl3 \
      zlib1g \
      libevent-dev \
 && rm -rf /var/*/apt \
 && mkdir -m 700 /data \
 && chown -R 65534:65534 /data

USER 65534:65534
VOLUME ["/data"]
EXPOSE 9050 9051

ENTRYPOINT ["/usr/local/bin/tor", "DataDirectory", "/data", "SocksPort", "0.0.0.0:9050", "ControlPort", "0.0.0.0:9051"]
