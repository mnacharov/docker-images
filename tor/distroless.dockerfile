ARG DEBIAN_VERSION=12.2
ARG DISTROLESS_VERSION=static-debian12
FROM debian:$DEBIAN_VERSION as build
LABEL org.opencontainers.image.authors="mike.nacharov@gmail.com"

WORKDIR /build
ARG ZLIB_VERSION=1.3
ARG OPENSSL_VERSION=3.2.0
ARG OPENSSL_ARCH=linux-x86_64
ARG LIBEVENT_VERSION=2.1.12
ARG VERSION=0.4.8.9

# python3 installed to run tor unittests before build
RUN apt-get update -qq \
 && apt-get upgrade -yqq \
 && apt-get install -yqq \
      build-essential \
      cmake \
      libzstd-dev \
      pkg-config \
      python3 \
      wget

##~ ZLib ~##
RUN wget -q https://github.com/madler/zlib/archive/refs/tags/v$ZLIB_VERSION.tar.gz \
 && tar -xf v$ZLIB_VERSION.tar.gz

RUN cd zlib-$ZLIB_VERSION/ \
 && ./configure --prefix=/build/dist --static \
 && make \
 && make install

##~ OpenSSL ~##
RUN wget -q https://github.com/openssl/openssl/archive/refs/tags/openssl-$OPENSSL_VERSION.tar.gz \
 && tar -xf openssl-$OPENSSL_VERSION.tar.gz

RUN cd openssl-openssl-$OPENSSL_VERSION \
 && ./config \
      no-shared \
      no-dso \
       --with-zlib-lib=/build/dist/lib/libz.a \
       --with-zlib-include=/build/dist/include \
      --prefix=/build/dist \
      $OPENSSL_ARCH \
 && make depend \
 && make \
 && make install \
 && cp /build/dist/include/openssl/* /build/dist/include \
 && cp -r /build/dist/lib64/* /build/dist/lib


###~ LibEvent ~##
RUN wget -q https://github.com/libevent/libevent/archive/refs/tags/release-$LIBEVENT_VERSION-stable.tar.gz \
 && tar -xf release-$LIBEVENT_VERSION-stable.tar.gz

RUN cd libevent-release-$LIBEVENT_VERSION-stable \
 && mkdir build \
 && cd build \
 && cmake \
      -DCMAKE_INSTALL_PREFIX=/build/dist \
      -DEVENT__LIBRARY_TYPE=STATIC \
      -DEVENT__DISABLE_SAMPLES=ON \
      -DEVENT__DISABLE_REGRESS=ON \
      -DOPENSSL_CRYPTO_LIBRARY=/build/dist/lib64/libcrypto.a \
      -DOPENSSL_INCLUDE_DIR=/build/dist/include/openssl \
      -DOPENSSL_SSL_LIBRARY=/build/dist/lib64/libssl.a \
      .. \
 && make \
 && make install

###~ Tor ~###
RUN wget -q https://dist.torproject.org/tor-$VERSION.tar.gz \
            https://dist.torproject.org/tor-$VERSION.tar.gz.sha256sum \
 && sha256sum -c tor-$VERSION.tar.gz.sha256sum \
 && tar -xf tor-$VERSION.tar.gz

RUN cd tor-$VERSION \
 && ./configure \
      --disable-asciidoc \
      --disable-html-manual \
      --disable-lzma \
      --disable-manpage \
      --disable-seccomp \
      --disable-system-torrc \
      --disable-systemd \
      --enable-fatal-warnings \
      --enable-static-tor \
      --prefix /build/tor \
      --with-libevent-dir=/build/dist \
      --with-openssl-dir=/build/dist \
      --with-zlib-dir=/build/dist \
 && make \
 && make install

RUN mkdir -m 700 tor/data && chown -R 65534:65534 tor/data

FROM gcr.io/distroless/$DISTROLESS_VERSION as run

COPY --from=build /build/tor /

USER 65534:65534
VOLUME ["/data"]
EXPOSE 9050 9051

ENTRYPOINT ["/bin/tor", "DataDirectory", "/data", "SocksPort", "0.0.0.0:9050", "ControlPort", "0.0.0.0:9051"]
